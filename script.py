import os
import json

module_dir_path = 'data_test/SoftStackCatalog/modulefiles/mugqic'
modules = os.listdir(os.path.join(os.getcwd(), module_dir_path))

with open('catalog.json', 'w') as f:
    data = {
        "name": "genpipes",
        "version": "3.1.6-beta",
        "modules": {

        }
    }

    for module in modules:
        versions = os.listdir(os.path.join(os.getcwd(), module_dir_path, module))
        versions.remove('.version')
        f_read = open(os.path.join(os.getcwd(), module_dir_path, module, ".version"), "r")
        current_ver = f_read.readlines()[-1].split(" ")[-1][1:-2]
        f_read.close()
        data["modules"][module] = {
            "available_versions": versions,
            "current_version": current_ver
        }
    json.dump(data, f, indent=4)
